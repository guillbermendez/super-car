'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  up:  (queryInterface, Sequelize) => {

    const salt = bcrypt.genSaltSync(10);

  return queryInterface.bulkInsert('Users', [
    {
      name: 'Josahandy',
      lastname: 'Avendaño',
      email: 'josahandyav@gmail.com',
      role: 'super',
      password: bcrypt.hashSync('123abc', salt),
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'Guillber',
      lastname: 'Mendez',
      email: 'guillmendez@gmail.com',
      password: bcrypt.hashSync('abc123', salt),
      role: 'user',
      createdAt: new Date(),
      updatedAt: new Date(),
    }
  ], {});
  },

  down: (queryInterface, Sequelize) => {
  return queryInterface.bulkDelete('Users', null, {});
  }
};
