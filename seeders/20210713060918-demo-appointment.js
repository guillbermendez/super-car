'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Appointments', [
      {
        ServicesId: 1,
        modelCar: "Camaro 2008" ,
        telephone: 2431246,
        commentary: "Por favor respondanme",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        ServicesId: 2,
        modelCar: "Aveo 2006" ,
        telephone: 2221234,
        commentary: "Espero pronto su respuesta",
        createdAt: new Date(),
        updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
