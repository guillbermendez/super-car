const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JWTstrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');

const {User} = require('../models');


const TOKEN_SECRET='phDqedd5a4sd5f4sadf$&$%&/R24dewr563';

passport.use('login', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
}, async (email, password, done) => {
  try {
    const user = await User.findOne({ where: { email, active: true } }); 
    if (!user) {
      return done(null, false, { message: 'Usuario no encontrado' });
    }

    const validado = await user.isValidPassword(password); 
    if (!validado) {
      return done(null, false, { message: 'Datos de acceso incorrectos' });
    }

    // si es valido, continuar con la petición, pasándole los datos del usuario autenticado
    return done(null, user);
  } catch (error) {
    return done(error);
  }
}));

passport.use(new JWTstrategy({
  
  secretOrKey: TOKEN_SECRET, //process.env.TOKEN_SECRET,
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
}, async (token, done) => {
  try {
    return done(null, { ...token.user, tokenExp: token.exp });
  } catch (error) {
    return done(error);
  }
}));