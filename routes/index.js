const express = require('express');


const router = express.Router();

// importar archivos de rutas
const usersRoutes = require('./usersRoutes');
const servicesRoutes = require('./servicesRoutes');
const appointmentsRoutes = require('./appointmentsRoutes');

module.exports = () => {

//vincular router de cada archivo de rutas
usersRoutes(router);
servicesRoutes(router);
appointmentsRoutes(router);

 return router;
}