const express = require('express');
const router = express.Router();

const webPushController = require('../controllers/WebPushController');
const sesionController = require('../controllers/SesionController');
const usersController = require('../controllers/UsersController');
const passwordController = require('../controllers/PasswordController');

module.exports = function() {
    // Rutas que no requieren autenticacion

    //rutas de notificaciones
    router.post('/push', webPushController.subscribe);

    router.post('/login', sesionController.login);
    router.post('/users', usersController.add);
    router.post('/reset-password', passwordController.resetearPassword);
    router.post('/validate-token', passwordController.validarToken);
    router.post('/update-password', passwordController.guardarNuevoPassword);

   
    return router;
}