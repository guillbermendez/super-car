const bcrypt = require('bcrypt');
const { User } = require('../models')
const {sendPushNotification}= require('../utils/notificationsPush');
//Agregar  Usuario
exports.add = async (request, response, next) => {
    try {
        // validar que venga la contraseña
    if (!request.body.password) {
        response.status(400).json({ error: true, message: 'La contraseña es obligatoria'});
        next();
    }

    // si si trae la contraseña proceder al registro
    const datosUsuario =  {...request.body};

    //Cifrar la contraseña
    const salt = await bcrypt.genSalt(10);
    datosUsuario.password = await bcrypt.hash(datosUsuario.password, salt);
    
    // guardar el ususario
    const usuario = await User.create(datosUsuario);

    //Evitar enviar la contraseña
    usuario.password = null; 

    //Notificacion activa
    const subscription = { endpoint:
        'https://fcm.googleapis.com/fcm/send/c-cmnDAb1OY:APA91bHxQmR76iH_HiXf_1drfIoeO41WKlQ9EZNZJwC2DUEQSV-p8PW61Aa8hfr8TV-3BWZCPRkyqUtqcK5-M1AnSNlb5YBQ8aGPaQ8vQp2azImtzDL4xA_adxRUDJ9VbcihLZYmCCnE',
       expirationTime: null,
       keys:
        { p256dh:
           'BOQtYn0fWdfAFxYty9YfS9-iITP1SwMgYWeVIrdzmMpM6BhDQF2JyoDdQAcFdlhKJPOjmb10BpulM6q-KebjAvg',
          auth: 'Ej5k4Twcxq-CqTWNj6_YDg' }
         };
    //recuperar subscripcion
     sendPushNotification(
        subscription,
        "Perfil Creado",
    `${request.body.name} bienvenido`,
        );

    response.json({ message: 'El usuario se registro', usuario, subscription})
    
    } catch (error) {
        console.log(error);
        let errores = [];
        if (error.errors) {
            errores = error.errors.map(errorItem => ({
                campo: errorItem.path,
                error: errorItem.message,
            }));
        }


        response.status(503).json({ error: true, message: 'Error al agregar usuario', errores,});
    }
};

//listar usuario
exports.listar = async(req, res, next) => {
    try {
        //extraer la lista de usuarios
        const usuarios = await User.findAll({});
        res.json(usuarios);
    } catch (error) {
        console.log(error);
        res.status(404).json({ mensaje: 'Error al leer el usuario'});
    }
};

//mostrar / leer / id
exports.mostrar = async(req, res, next) => {
    try {
        //Buscar el registro, por id
        const usuario = await User.findByPk(req.params.id);
        usuario.password = null; 
        if (!usuario) {
            res.status(404).json({error: true, message: 'No se encontró el usuario'});
        } else {
            res.json(usuario);
        }
    } catch (error) {
        console.log(error);
        response.status(503).json({ message: 'Error al leer el usuario'});
    }
};

//Actualizar
exports.actualizar = async(req, res, next) => {
    try {
        //Obtener el registro de la categoria desde la bd
        const usuario = await User.findByPk(req.params.id);
        if (!usuarios) {
            res.status(404).json({message: 'No se encontro el usuario. '});
        } else {
            /*actualizar la bd
            categorias.nombre = req.body.nombre;
            categorias.activo = req.body.activo;
            el resto de las propiedades
            Procesar las propiedades que vienen en el body*/
            Object.keys(req.body).forEach((propiedad) => {
                usuario[propiedad] = req.body[propiedad];
            })

            //guardar los cambios
            await usuario.save();
            res.json({ message: 'El registro fue actualizado'});
        }
    } catch (error) {
        console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        res.status(503).json({ 
            error: true,
            message: 'Error al actualizar la usuario',
            errores,
        });
    }
};


//Eliminar usuario
exports.eliminar = async(req, res, next) => {
    try {
        //Eliminar registro, por id
        const usuarios = await User.findByPk(req.params.id);
        if (!usuarios) {
            res.status(404).json({error:true, message: 'No se encontró el usuario'});
        } else {
            await usuarios.destroy();
            res.json({ mensaje: 'Se eliminó el usuario. '});
        }
    } catch (error) {
        res.status(503).json({ message: 'Error al eliminar el usuario.'});
    }
};
