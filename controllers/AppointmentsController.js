const { Appointment } = require('../models');
const {sendPushNotification}= require('../utils/notificationsPush');
//Agregar  Appointment
exports.add = async(req, res, next) => {
    try {
        //asociar
        const datosAppointment = {...req.body};
        datosAppointment.ServicesId = req.params.services;        
        
        //crear un Appointment
       const appointment = await Appointment.create(datosAppointment);
       const subscription = { endpoint:
        'https://fcm.googleapis.com/fcm/send/c-cmnDAb1OY:APA91bHxQmR76iH_HiXf_1drfIoeO41WKlQ9EZNZJwC2DUEQSV-p8PW61Aa8hfr8TV-3BWZCPRkyqUtqcK5-M1AnSNlb5YBQ8aGPaQ8vQp2azImtzDL4xA_adxRUDJ9VbcihLZYmCCnE',
       expirationTime: null,
       keys:
        { p256dh:
           'BOQtYn0fWdfAFxYty9YfS9-iITP1SwMgYWeVIrdzmMpM6BhDQF2JyoDdQAcFdlhKJPOjmb10BpulM6q-KebjAvg',
          auth: 'Ej5k4Twcxq-CqTWNj6_YDg' }
         };
      //recuperar subscripcion
      sendPushNotification(
        subscription,
        "Perfil Creado",
        `${req.body.name} bienvenido`,
        );

        //devolver appointment creado
        res.json({mensaje: 'Se agregó la cita.', appointment, subscription,});
       
    } catch (error) { 
        console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        res.status(503).json({ 
            error: true,
            message: 'Error al agregar cita.',
            errores,
        });
        next();
    }
};

//listar Appointment
exports.list = async(req, res, next) => {
    try {
        //extraer la lista de Appointments
        const appointment = await Appointment.findAll({ 
            where: { ServicesId: req.params.services }
       });
       res.json(appointment);
    } catch (error) {
        res.status(503).json({ mensaje: 'Error al leer cita'});
    }
};

//mostrar / leer / :id
exports.show = async(req, res, next) => {
    try {
        //Buscar el registro, por id
        const appointment = await Appointment.findByPk(req.params.id);
        if (!appointment) {
            res.status(404).json({ error: true, message: 'No se encontró la cita'});
        } else {
            res.json(appointment);
        }
    } catch (error) {
        res.status(503).json({message: 'Error al leer cita'});
        console.error(error);
    }
};
//Actualizar appointment
exports.update = async(req, res, next) => {
    try {
        //Obtener el registro de los servicios desde la bd
        const appointment = await Appointment.findOne({
            where: { ServicesId: req.params.services, id: req.params.id}
        });
        if (!appointment) {
            res.status(404).json({ message: 'No se encontró la cita. '});
        } else {
            Object.keys(req.body).forEach((propiedad) => {
                appointment[propiedad] = req.body[propiedad];
            })
            //guardar los cambios
            await appointment.save();
            res.json({ 
                message: 'El registro fue actualizado', 
                appointment,
            });
        }
    } catch (error) {
        console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        res.status(503).json({ 
            error: true,
            message: 'Error al actualizar la cita.',
            errores,
        });
    }
};
//Eliminar apointment
exports.delete = async(req, res, next) => {
    try {
        //Eliminar registro, por id
        const appointment = await Appointment.findOne({ 
            where: { ServicesId: req.params.services, id: req.params.id}
        });
        if (!appointment) {
            res.status(404).json({ message: 'No se encontró la cita. '});
        } else {
            await appointment.destroy();
            res.json({ mensaje: 'Se eliminó la cita. '});
        }
    } catch (error) {
        res.status(503).json({ mensaje: 'Error al eliminar la cita.'});
    }
};
