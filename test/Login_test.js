let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:5000';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7ImlkIjo2LCJlbWFpbCI6Impvc2FoYW5keWF2QGdtYWlsLmNvbSIsInJvbGUiOiJzdXBlciJ9LCJpYXQiOjE2MjgzNjQ3NjgsImV4cCI6MTYyODUzNzU2OH0.V1Fbrb5T5Fcp25wla80DZDMykspfBYP-jIb-JhkBo24';

describe("Registro, autenticación de usuarios y restablecer contraseña", () => {
// caso de prueba
describe("Registro de usuarios", () => {
  it("Debe registrar nuevo usuario", (done) => {
    chai.request(url)
    .post('/users')
    .send({
        name: "Juana4",
        lastname: "Gomez4",
        email: "juanagomez4@gmail.com",
        password: "123",
    })
    .end(function(err, res) {
      //console.log(res);
      expect(res).to.have.status(200);
      expect(res.body).to.have.property('usuario');
      expect(res.body).to.have.property('message');
      expect(res.body).to.have.property('subscription');
      done();
    });
    });
    it("Debe rechazar el registro de usuario existente", (done) => {
        chai.request(url)
        .post('/users')
        .send({
          name: 'Josahandy',
          lastname: 'Avendaño',
          email: 'josahandyav@gmail.com',
          password: '123abc'
        })
        .end(function(err, res) {
          //console.log(res);
          expect(res).to.have.status(503);
          expect(res.body).to.have.property('error');
          expect(res.body).to.have.property('message');
          done();
        });
    });
    it('Debe rechazar crear usuario sin contraseña', (done) => {
      chai.request(url)
      .post('/users')
      .send({
          name: 'Angel',
          lastname: 'Lopez',
          email: 'lopez2@gmail.com',
          role: 'usuario',
      })
      .end((err, res)  => {
          expect(res).to.have.status(400);
          done();
      })
    });
  });
  describe("Autenticar usuario", () => {
  it("Debe autenticar un usuario", (done) => {
      chai.request(url)
      .post('/login')
      .send({
        email: "josahandyav@gmail.com",
        password: "123abc",
      })
      .end((err, res) =>{
        expect(res).to.have.status(200)
        expect(res.body).to.have.property('user');
        expect(res.body).to.have.property('token');
        done();
      })
    });
  it("Si el email no existe:'Usuario no encontrado'", (done) => {
    chai.request(url)
    .post('/login')
    .send({
      email: "ejemplo@gmail.com",
      password: "abc123",
    })
    .end((err, res) => {
      //console.log(res);
      expect(res).to.have.status(400);
      expect(res.body).to.have.property('message');
      done();
    })
  });
  it("Si el password es incorrecto:'Datos de acceso incorrectos'", (done) => {
    chai.request(url)
    .post('/login')
    .send({
      email: "guillmendez@gmail.com",
      password: "123wre",
    })
    .end((err, res) => {
      //console.log(res);
      expect(res).to.have.status(400);
      expect(res.body).to.have.property('message');
      done();
    })
  })
  });
  describe("Restablecer contraseña", () => {
    it("Debe enviar un mensaje al email proporcionado", (done) => {
      chai.request(url)
      .post('/reset-password')
      .send({
        email: "guillbermndz@gmail.com"
      })
      .end((err, res) =>{
        expect(res).to.have.status(404)
        expect(res.body).to.have.property('message');
        done();
      })
    });
    it("Si el token es valido: 'Ingrese su nueva contraseña'", (done) => {
      chai.request(url)
      .post('/validate-token')
      .send({
        token: "$2b$10$uEVolOQEln3lFZs7y-VQiOD-Kr1Vq6GBKkjrdIBgiVA2aAO9wkTAW",
      })
      .end((err, res)=>{
        //console.log(res)
        expect(res).to.have.status(400)
        //expect(res.body).to.have.property('isValid');
        expect(res.body).to.have.property('message');
        done();
      })
    });
    it("Guardar la nueva contraseña", (done) => {
      chai.request(url)
      .post('/update-password')
      .send({
        token: "$2b$10$uEVolOQEln3lFZs7y-VQiOD-Kr1Vq6GBKkjrdIBgiVA2aAO9wkTAW",
        password: "new123"
      })
      .end((err, res) => {
        //console.log(res)
        expect(res).to.have.status(400);
        expect(res.body).to.have.property('message');
        done();
      })
    })
  });
});
