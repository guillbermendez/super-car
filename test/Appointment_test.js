let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:5000';

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7ImlkIjo2LCJlbWFpbCI6Impvc2FoYW5keWF2QGdtYWlsLmNvbSIsInJvbGUiOiJzdXBlciJ9LCJpYXQiOjE2Mjk1MTkxOTMsImV4cCI6MTYyOTY5MTk5M30.7Qp30Aalt80I-bznVHAU0_liWqhYmKpff07f2O-tZYA';

describe('Test de citas', () => {

    describe('Agregar cita', () => {
        it('Debe agregar una cita en un servicio específico', (done) => {
            chai.request(url)
            .get('/services')
            .set({'Authorization': `jwt ${token}`})
            .end((err, res) => {

                idService = res.body[res.body.length - 1].id;

                chai.request(url)
                .post(`/services/${2}/appointments`)
                .set({'Authorization': `jwt ${token}`})
                .send({
                    modelCar: "Explorer 2001" ,
                    telephone: 2221246,
                    commentary: "Espero su respuesta.",
                })
                .end((err, res) => {
                    //console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('appointment');
                    expect(res.body).to.have.property('subscription');
                    done();
                });
            });
        });
        it('Debe rechazar crear cita del usuario cuando el token sea inválido', (done) => {
            chai.request(url)
            .post('/services/1/appointments')
            .set({ 'Authorization': `jwt ${token}X`})
            .send({
                modelCar: "Chevy 2008" ,
                telephone: 2221246,
                commentary: "Espero su respuesta.",
            })
            .end((err, res)  => {
                expect(res).to.have.status(401);
                done();
            })
        });
        it('Debe rechazar crear cita sin modelo de carro', (done) => {
            chai.request(url)
            .post('/services/1/appointments')
            .set({ 'Authorization': `jwt ${token}`})
            .send({
               telephone: 2221246,
               commentary: "Espero su respuesta.",
            })
            .end((err, res)  => {
                expect(res).to.have.status(503);
                expect(res.body).to.have.property('error').to.be.equal(true);
                done();
            })
        });
    });

    describe('Leer cita', () => {
        let idAppointment = 0;
        it('Debe devolver todas las citas del usuario', (done) => {
            chai.request(url)
            .get('/services/2/appointments')
            .set({ 'Authorization': `jwt ${token}`})
            .end((err, res)  => {
                if(res.body.length > 0){
                  idAppointment = res.body[0].id;
                }
                expect(res).to.have.status(200);
                done();
            })
        });
        it('Debe devolver los datos de una cita de usuario', (done) => {
            chai.request(url)
            .get(`/services/2/appointments/${idAppointment}`)
            .set({ 'Authorization': `jwt ${token}`})
            .end((err, res)  => {
                console.log(res.body);
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('modelCar');
                done();
            })
        });
        it('No debe devolver los datos de una cita que no existe', (done) => {
            chai.request(url)
            .get(`/services/2/appointments/${idAppointment * 50}`)
            .set({ 'Authorization': `jwt ${token}`})
            .end((err, res)  => {
                console.log(res.body);
                expect(res).to.have.status(404);
                expect(res.body).to.have.property('error').to.be.equal(true);
                expect(res.body).to.have.property('message').to.be.equal('No se encontró la cita');
                done();
            })
        });
    });

    describe('Actualizar cita', () => {
        it('Debe actualizar una cita en un servicio específico', (done) => {
                chai.request(url)
                .get('/services')
                .set({'Authorization': `jwt ${token}`})
                .end((err, res) => {
    
                    idService = res.body[res.body.length - 1].id;
                    idCita = 3;
                    chai.request(url)
                    .put(`/services/${3}/appointments/${idCita}`)
                    .set({'Authorization': `jwt ${token}`})
                    .send({
                        modelCar: "Cavalier 2001" ,
                        telephone: 24321246,
                        commentary: "Espero su respuesta",
                    })
                    .end((err, res) => {
                        console.log(res.body);
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('appointment');
                        done();
                    });
                });
        });
        it('Debe rechazar actualizar cita del usuario cuando el token sea inválido', (done) => {
            chai.request(url)
            .put('/services/1/appointments/2')
            .set({ 'Authorization': `jwt ${token}X`})
            .send({
                modelCar: "Explorer 2008" ,
                telephone: 2431152217,
                commentary: "Espero su respuesta, pronto.",
            })
            .end((err, res)  => {
                expect(res).to.have.status(401);
                done();
            })
        });
        it('Debe rechazar actualizar cita sin modelo de carro', (done) => {
            chai.request(url)
            .put('/services/1/appointments/1')
            .set({ 'Authorization': `jwt ${token}`})
            .send({
               telephone: 2221246,
               commentary: "Espero su respuesta.",
            })
            .end((err, res)  => {
                expect(res).to.have.status(200);
                done();
            })
        });
    })
    describe('Eliminar cita', () => {
        it('Debe eliminar una cita en un servicio específico', (done) => {
            chai.request(url)
            .get('/services')
            .set({'Authorization': `jwt ${token}`})
            .end((err, res) => {

                idService = res.body[res.body.length - 1].id;
                idCita = 17;
                chai.request(url)
                .delete(`/services/${1}/appointments/${idCita}`)
                .set({'Authorization': `jwt ${token}`})
                .end((err, res) => {
                    console.log(res.body);
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('mensaje');
                    done();
                });
            });
        });
        it('Debe rechazar eliminar cita del usuario cuando el token sea inválido', (done) => {
            chai.request(url)
            .delete('/services/1/appointments/1')
            .set({ 'Authorization': `jwt ${token}y`})
            .end((err, res)  => {
                expect(res).to.have.status(401);
                done();
            })
        });
        it('Debe rechazar eliminar cita, si la cita no existe', (done) => {
            chai.request(url)
            .delete('/services/1/appointments/5')
            .set({ 'Authorization': `jwt ${token}`})
            .end((err, res)  => {
                expect(res).to.have.status(404);
                expect(res.body).to.have.property('message');
                done();
            })
        });
    });
});
