const webpush = require('web-push');

//Configuracion
webpush.setVapidDetails ( 
    `mailto:${process.env.PUSH_EMAIL}`, 
     process.env.PUSH_PUBLIC_KEY, 
     process.env.PUSH_PRIVATE_KEY,
  );

module.exports = webpush; 