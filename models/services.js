'use strict';

module.exports = (sequelize, DataTypes) => {
  const Services = sequelize.define('Services', {
    name: DataTypes.STRING,
    cost: DataTypes.STRING,
    time: DataTypes.STRING
  }, {
    /*defaultScope: {
      attributes: ['id', 'name', 'cost', 'time'],
    }*/
    defaultScope: {
      attributes: { exclude: ['createdAt', 'updatedAt'] },
    }
  });
  
  Services.associate = function(models) {
    models.Services.hasMany(models.Appointment, {
      as: 'Appointments',
    });
  };

  return Services;
};